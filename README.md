# Credit Card App

This service allows you to perform operations related to credit cards.

## Database Setup

To set up the database corresponding to this application, use the SQL script `creditcard.sql` located at `credit-card-validator\src\main\resources\static`. This script includes instructions to create the necessary tables ( CreditCards and Users ) and insert sample data for testing the credit card service.

## Credit Card Endpoints Usage

Use appropriate HTTP methods (GET, POST, PUT, DELETE) along with the provided endpoints to interact with the credit card service.

- To validate a credit card: `POST http://localhost/creditcard/operations/validate` (include credit card details in the request body)
- To get all credit cards: `GET http://localhost/creditcard/operations`
- To get a specific credit card: `GET http://localhost/creditcard/operations/{id}`
- To create a new credit card: `POST http://localhost/creditcard/operations` (include credit card details in the request body)
- To update a credit card: `PUT http://localhost/creditcard/operations/{id}` (include updated credit card details in the request body)
- To delete a credit card: `DELETE http://localhost/creditcard/operations/{id}`

## User Endpoints Usage

Utilize appropriate HTTP methods (GET, POST, PUT, DELETE) with the provided endpoints to interact with the user service.

Example:
- To retrieve a user by username: `GET http://localhost/users/{username}`
- To create a new user: `POST http://localhost/users` (include user details in the request body)
- To update an existing user: `PUT http://localhost/users/{username}` (include updated user details in the request body)
- To delete a user: `DELETE http://localhost/users/{username}`
