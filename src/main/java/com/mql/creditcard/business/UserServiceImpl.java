package com.mql.creditcard.business;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mql.creditcard.dao.UserRepository;
import com.mql.creditcard.models.User;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public User getUser(String username) {
		Optional<User> optionalUser = userRepository.findById(username);
		return optionalUser.orElse(null);
	}

	@Override
	public User createUser(User user) {
		return userRepository.save(user);
	}

	@Override
	public User updateUser(String username, User user) {
		Optional<User> optionalUser = userRepository.findById(username);
		if(optionalUser.isPresent()) {
			User exisitingUser = optionalUser.get();
			exisitingUser.setUsername(user.getUsername());
			exisitingUser.setPassword(user.getPassword());
			return userRepository.save(exisitingUser);
		}
		return null;
	}

	@Override
	public boolean deleteUser(String username) {
		Optional<User> optionalUser = userRepository.findById(username);
		if(optionalUser.isPresent()) {
			userRepository.delete(optionalUser.get());
			return true;
		}
		return false;
	}
}
