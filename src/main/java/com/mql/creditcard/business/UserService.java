package com.mql.creditcard.business;

import com.mql.creditcard.models.User;

public interface UserService {
	public User getUser(String username);
	public User createUser(User user);
	public User updateUser(String username,User user);
	public boolean deleteUser(String username);
}