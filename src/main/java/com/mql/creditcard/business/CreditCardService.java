package com.mql.creditcard.business;

import java.util.List;

import com.mql.creditcard.models.CreditCard;

public interface CreditCardService {
	public boolean isValidCreditCard(CreditCard creditCard);
	public boolean isValidCreditCard(String creditCardNumber);
	public List<CreditCard> getAllCreditCards();
	public CreditCard getCreditCardById(Long id);
	public CreditCard createCreditCard(CreditCard creditCard);
	public CreditCard updateCreditCard(Long id, CreditCard creditCard);
	public boolean deleteCreditCard(Long id);
}