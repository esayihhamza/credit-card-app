package com.mql.creditcard.business;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mql.creditcard.dao.CreditCardRepository;
import com.mql.creditcard.models.CreditCard;
import com.mql.creditcard.utils.ValidationUtils;

@Service
public class CreditCardServiceImpl implements CreditCardService {
    @Autowired
    private CreditCardRepository creditCardRepository;
    
    @Override
    public boolean isValidCreditCard(CreditCard creditCard) {
    	return ValidationUtils.checkLuhn(creditCard.getNumber());
    }
    
    @Override
    public boolean isValidCreditCard(String creditCardNumber) {
    	System.out.println("HERE : " + creditCardNumber);
    	return ValidationUtils.checkLuhn(creditCardNumber);
    }
    
    @Override
    public List<CreditCard> getAllCreditCards() {
        return creditCardRepository.findAll();
    }

    @Override
    public CreditCard getCreditCardById(Long id) {
        Optional<CreditCard> optionalCreditCard = creditCardRepository.findById(id);
        return optionalCreditCard.orElse(null);
    }

    @Override
    public CreditCard createCreditCard(CreditCard creditCard) {
    	if(ValidationUtils.checkLuhn(creditCard.getNumber())) { // Luhn's algorithm is used here to check the cards' validation before creation	
    		return creditCardRepository.save(creditCard);
    	}
    	else {
    		return null;
    	}
    }

    @Override
    public CreditCard updateCreditCard(Long id, CreditCard updatedCard) {
        Optional<CreditCard> optionalCreditCard = creditCardRepository.findById(id);
        if (optionalCreditCard.isPresent() && ValidationUtils.checkLuhn(updatedCard.getNumber())) { // Luhn' algorithm is used here as well
            CreditCard existingCard = optionalCreditCard.get();
            existingCard.setNumber(updatedCard.getNumber());
            existingCard.setExpiryDate(updatedCard.getExpiryDate());
            existingCard.setControlNumber(updatedCard.getControlNumber());
            existingCard.setType(updatedCard.getType());
            return creditCardRepository.save(existingCard);
        }
        return null;
    }

    @Override
    public boolean deleteCreditCard(Long id) {
        Optional<CreditCard> optionalCreditCard = creditCardRepository.findById(id);
        if (optionalCreditCard.isPresent()) {
            creditCardRepository.delete(optionalCreditCard.get());
            return true;
        }
        return false;
    }
}
