package com.mql.creditcard.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mql.creditcard.models.User;

public interface UserRepository extends JpaRepository<User, String>{}