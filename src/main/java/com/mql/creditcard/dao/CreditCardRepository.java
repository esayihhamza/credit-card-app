package com.mql.creditcard.dao;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import com.mql.creditcard.models.CreditCard;

public interface CreditCardRepository extends JpaRepository<CreditCard, String>{
	Optional<CreditCard> findById(Long id);
}
