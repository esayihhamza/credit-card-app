package com.mql.creditcard.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mql.creditcard.business.CreditCardService;
import com.mql.creditcard.models.CreditCard;

@RestController
@RequestMapping("/operations")
@CrossOrigin(origins = "*")
public class CreditCardResource {
	@Autowired
	private CreditCardService creditCardService;

//	@PostMapping("/validate")
//	public ResponseEntity<Boolean> checkCreditCard(@RequestBody String creditCardNumber) {
//		boolean isValid = creditCardService.isValidCreditCard(creditCardNumber);
//		if (isValid) {
//			return new ResponseEntity<>(true, HttpStatus.OK);
//		}
//		return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
//	}

	@PostMapping("/validate")
	public ResponseEntity<Boolean> checkCreditCard(@RequestBody CreditCard creditCard) {
		boolean isValid = creditCardService.isValidCreditCard(creditCard);
		if (isValid) {
			return new ResponseEntity<>(true, HttpStatus.OK);
		}
		return new ResponseEntity<>(false,HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<List<CreditCard>> getCreditCards() {
		List<CreditCard> creditCardList = creditCardService.getAllCreditCards();

		if (creditCardList.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(creditCardList, HttpStatus.OK);
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<CreditCard> getCreditCardById(@PathVariable Long id) {
		CreditCard creditCard = creditCardService.getCreditCardById(id);

		if (creditCard != null) {
			return new ResponseEntity<>(creditCard, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping
	public ResponseEntity<CreditCard> createCreditCard(@RequestBody CreditCard creditCard) {
		CreditCard createdCard = creditCardService.createCreditCard(creditCard);

		if (createdCard != null) {
			return new ResponseEntity<>(createdCard, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("/{id}")
	public ResponseEntity<CreditCard> updateCreditCard(@PathVariable Long id, @RequestBody CreditCard creditCard) {
		CreditCard updatedCard = creditCardService.updateCreditCard(id, creditCard);

		if (updatedCard != null) {
			return new ResponseEntity<>(updatedCard, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteCreditCard(@PathVariable Long id) {
		boolean deleted = creditCardService.deleteCreditCard(id);

		if (deleted) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
